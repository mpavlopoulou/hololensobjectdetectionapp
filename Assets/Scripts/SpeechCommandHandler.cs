﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SpeechCommandHandler : MonoBehaviour
{
    public void ChangeScene(string sceneName) => SceneManager.LoadScene(sceneName);

    public void CaptureImageForAnalysis()
    {
        // Only if the object detection process does not start automatically
        if (!ObjectDetectionSceneOrganiser.Instance.objectDetectionProcessIsAutomatic)
        {
            ImageCapture.Instance.StartImageCaptureProcess();
        }
    }
}
