﻿using System.Collections.Generic;
using UnityEngine;

// Domain model for YOLO Object detection results
public class YoloDetectionDomainModel : MonoBehaviour
{
    public class YOLOObjectDetectionDomainResult
    {
        public IList<YOLODomainPrediction> Predictions { get; set; }
        public string AudioPredictionsDescription { get; set; }

        public YOLOObjectDetectionDomainResult(IList<YOLODomainPrediction> predictions)
        {
            Predictions = predictions;
        }

    }

    public class YOLODomainPrediction
    {
        // The detection probability
        public double Probability { get; set; }
        // The detection class name
        public string TagName { get; set; }

        // Bounding box info (center and four corners in world 3D position)
        public Vector3 DetectionCenterWorldPosition { get; set; }

        public int BoundingBoxCornersNumber { get; set; }
        public Vector3[] DetectionBoundingBoxCornerWorldCoordinates { get; set; }

        public YOLODomainPrediction(double probability, string tagName,
            Vector3 centerPosition, Vector3 topLeftPosition, Vector3 topRightPosition, Vector3 bottomLeftPosition, Vector3 bottomRightPosition)
        {
            Probability = probability;
            TagName = tagName;
            DetectionCenterWorldPosition = centerPosition;

            BoundingBoxCornersNumber = 4;
            DetectionBoundingBoxCornerWorldCoordinates = new Vector3[BoundingBoxCornersNumber];
            // Add the coordinates in a sequence that will form a rectangle
            DetectionBoundingBoxCornerWorldCoordinates[0] = topLeftPosition;
            DetectionBoundingBoxCornerWorldCoordinates[1] = topRightPosition;
            DetectionBoundingBoxCornerWorldCoordinates[2] = bottomRightPosition;
            DetectionBoundingBoxCornerWorldCoordinates[3] = bottomLeftPosition;
        }
    }

}
