﻿using System;
using UnityEngine;

// API model for YOLO Object detection request
// Contains the camera frame to base64 format and a flag to denote if audio description of results should be returned
public class ImageAnalysisRequest : MonoBehaviour
{
    [Serializable]
    public partial class ImageAnalysisRequestObject
    {
        public String image;

        public bool predictionsAudioDescriptionEnabled;
    }
}