﻿using System;
using System.Collections.Generic;
using UnityEngine;

// API model for YOLO Object detection results
public class YOLODetectionAPIModel : MonoBehaviour
{

    [Serializable]
    public class YOLOObjectDetectionAPIResult
    {
        public List<YOLOAPIPrediction> predictions;

        public string predictionsAudioDescription;

        public static YOLOObjectDetectionAPIResult CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<YOLOObjectDetectionAPIResult>(jsonString);
        }
    }

    [Serializable]
    public class YOLOAPIPrediction
    {
        public double probability;

        public string tagName;

        public YOLOAPIBoundingBox boundingBox;

        public Vector2 centerCoordinates;

        public Vector2 topLeftCoordinates;
        public Vector2 topRightCoordinates;
        public Vector2 bottomLeftCoordinates;
        public Vector2 bottomRightCoordinates;

        public void SetCoordinatesOfPrediction()
        {
            float centerXCoordinate = (float)(boundingBox.left + (0.5 * boundingBox.width));
            float centerYCoordinate = (float)(boundingBox.top + (0.5 * boundingBox.height));
            centerCoordinates = new Vector2(centerXCoordinate, centerYCoordinate);

            double boundingBoxRight = boundingBox.left + boundingBox.width;
            double boundingBoxBottom = boundingBox.top + boundingBox.height;

            topLeftCoordinates = new Vector2((float)boundingBox.left, (float)boundingBox.top);
            topRightCoordinates = new Vector2((float)boundingBoxRight, (float)boundingBox.top);
            bottomLeftCoordinates = new Vector2((float)boundingBox.left, (float)boundingBoxBottom);
            bottomRightCoordinates = new Vector2((float)boundingBoxRight, (float)boundingBoxBottom);
        }
    }


    [Serializable]
    public partial class YOLOAPIBoundingBox
    {
        public double left;

        public double top;

        public double width;

        public double height;
    }

}
