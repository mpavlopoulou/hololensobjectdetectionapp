﻿// API model for Camera frames similarity check results
using System;
using UnityEngine;

public class CameraFramesSimilarityAPIModel : MonoBehaviour
{

    [Serializable]
    public class CameraFramesSimilarityAPIResult
    {
        public Boolean areFramesSimilar;

        public static CameraFramesSimilarityAPIResult CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<CameraFramesSimilarityAPIResult>(jsonString);
        }
    }
}