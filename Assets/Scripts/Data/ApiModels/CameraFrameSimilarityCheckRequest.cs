﻿using System;
using UnityEngine;

// API model for Camera frames similarity check request
// Contains the current and the previous camera frames to base64 format for comparison
public class CameraFrameSimilarityCheckRequest : MonoBehaviour
{
    [Serializable]
    public partial class CameraFrameSimilarityCheckRequestObject
    {
        public String previous_frame_image;

        public String current_frame_image;

    }
}