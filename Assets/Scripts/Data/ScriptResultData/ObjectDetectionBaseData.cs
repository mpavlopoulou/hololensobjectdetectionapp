﻿using UnityEngine;

public class ObjectDetectionBaseData
{
    public Resolution CameraResolution { get; protected set; }
    public Matrix4x4 CameraToWorldMatrix { get; protected set; }
    public Matrix4x4 ProjectionMatrix { get; protected set; }


    public ObjectDetectionBaseData(Resolution cameraResolution, Matrix4x4 cameraToWorldMatrix, Matrix4x4 projectionMatrix)
    {
        CameraResolution = cameraResolution;
        CameraToWorldMatrix = cameraToWorldMatrix;
        ProjectionMatrix = projectionMatrix;
    }
}
