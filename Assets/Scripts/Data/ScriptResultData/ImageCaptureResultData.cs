﻿using System.Collections.Generic;
using UnityEngine;

// Result data of Image capture process to be sent for analysis.

// The result is a JPEG buffer contents, the camera resolution and a copy of the camera's transform.
// The resolution is needed to calculate the height/width ratio of the picture.
// Camera to world and projection matrix are needed for the object detection results correct positioning from 2D Image to 3D World space

public class ImageCaptureResultData : ObjectDetectionBaseData
{
    public IList<byte> Image { get; protected set; }

    public ImageCaptureResultData(IList<byte> image, Resolution cameraResolution, Matrix4x4 cameraToWorldMatrix, Matrix4x4 projectionMatrix) :
        base(cameraResolution, cameraToWorldMatrix, projectionMatrix)
    {
        Image = image;
    }
}