﻿using UnityEngine;
using static YoloDetectionDomainModel;

// Result data of Object detection analysis process to be sent to scene organiser for display.
// YoloDetectionDomainModel is the domain model that holds predictions that are received from our analysis system
// cameraResolution and camera to world/projection matix are passed as retrieved from ImageCaptureResultData
public class ObjectDetectionAnalysisResultData : ObjectDetectionBaseData
{
    public YOLOObjectDetectionDomainResult DetectionDomainResult { get; protected set; }

    public ObjectDetectionAnalysisResultData(YOLOObjectDetectionDomainResult domainResult, Resolution cameraResolution, Matrix4x4 cameraToWorldMatrix, Matrix4x4 projectionMatrix) :
        base(cameraResolution, cameraToWorldMatrix, projectionMatrix)
    {
        DetectionDomainResult = domainResult;
    }
}