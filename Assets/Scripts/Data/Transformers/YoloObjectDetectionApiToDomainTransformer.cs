﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static YOLODetectionAPIModel;
using static YoloDetectionDomainModel;

namespace Transformers
{
    public static class YoloObjectDetectionApiToDomainTransformer
    {
        public static YOLOObjectDetectionDomainResult TransformYoloObjectDetectionResult(YOLOObjectDetectionAPIResult apiResult,
            Resolution cameraResolution, Matrix4x4 cameraToWorldMatrix, Matrix4x4 projectionMatrix)
        {

            IList<YOLODomainPrediction> domainPredictionsList = new List<YOLODomainPrediction>();

            IList<YOLOAPIPrediction> apiPredictionsList = apiResult.predictions;

            foreach (var apiPrediction in apiPredictionsList)
            {
                // Set the center of prediction coordinates in 2D space
                apiPrediction.SetCoordinatesOfPrediction();

                // Get the center of prediction in 3D space
                bool isCenterPositionValid = CoordinateTransfer.ImagePositionToWorldPosition(apiPrediction.centerCoordinates, cameraResolution, projectionMatrix, cameraToWorldMatrix, out Vector3 centerWorldPosition);

                // Get the four corners of the prediction bounding box in 3D space 
                bool isTopLeftPositionValid = CoordinateTransfer.ImagePositionToWorldPosition(apiPrediction.topLeftCoordinates, cameraResolution, projectionMatrix, cameraToWorldMatrix, out Vector3 topLeftWorldPosition);
                bool isTopRightPositionValid = CoordinateTransfer.ImagePositionToWorldPosition(apiPrediction.topRightCoordinates, cameraResolution, projectionMatrix, cameraToWorldMatrix, out Vector3 topRightWorldPosition);
                bool isBottomLeftPositionValid = CoordinateTransfer.ImagePositionToWorldPosition(apiPrediction.bottomLeftCoordinates, cameraResolution, projectionMatrix, cameraToWorldMatrix, out Vector3 bottomLeftWorldPosition);
                bool isBottomRightPositionValid = CoordinateTransfer.ImagePositionToWorldPosition(apiPrediction.bottomRightCoordinates, cameraResolution, projectionMatrix, cameraToWorldMatrix, out Vector3 bottomRightWorldPosition);

                // Check if all 3D positions are valid, then add a domain result to the list
                if (isCenterPositionValid && isTopLeftPositionValid && isTopRightPositionValid && isBottomLeftPositionValid && isBottomRightPositionValid)
                {
                    YOLODomainPrediction domainPrediction = new YOLODomainPrediction(apiPrediction.probability, apiPrediction.tagName, centerWorldPosition,
                        topLeftWorldPosition, topRightWorldPosition, bottomLeftWorldPosition, bottomRightWorldPosition);
                    domainPredictionsList.Add(domainPrediction);
                }

            }

            YOLOObjectDetectionDomainResult result = new YOLOObjectDetectionDomainResult(domainPredictionsList)
            {
                // Set the audio description result from server response
                AudioPredictionsDescription = apiResult.predictionsAudioDescription + GetObjectDetectionResultsDistancesDescription(domainPredictionsList)
            };

            return result;
        }


        private static string GetObjectDetectionResultsDistancesDescription(IList<YOLODomainPrediction> domainPredictionsList)
        {
            string distancesText = "";

            Vector3 headPosition = Camera.main.transform.position;

            foreach (var domainPrediction in domainPredictionsList)
            {
                float distance = Vector3.Distance(domainPrediction.DetectionCenterWorldPosition, headPosition);
                float distanceWithTwoDecimals = (float)Math.Round(distance * 100f) / 100f;

                distancesText += " The " + domainPrediction.TagName + " is " + distanceWithTwoDecimals.ToString() + " meters far away.";
            }

            return distancesText;
        }

    }
}
