﻿using System;
using System.Collections;
using System.Linq;
using Transformers;
using UnityEngine;
using UnityEngine.Networking;
using static ImageAnalysisRequest;
using static YOLODetectionAPIModel;
using static YoloDetectionDomainModel;

public class YoloObjectDetectionAnalyser : MonoBehaviour
{
    // Unique instance of this class
    public static YoloObjectDetectionAnalyser Instance;

    //Jetson TX2 prediction endpoint
    private readonly string predictionEndpoint = "http://tesla2.di.uoa.gr:8081/HoloLensObjectDetection/detectObjects";


    // Initializes this class
    private void Awake()
    {
        // Allows this instance to behave like a singleton
        Instance = this;
    }


    // Call our YOLO Object detection Service to submit the image.
    // Obtains the results of the analysis of the image, captured by the ImageCapture class.
    public IEnumerator AnalyseLastImageCaptured(ImageCaptureResultData imageCaptureResultData)
    {
        Debug.Log("Analyzing...");

        // Play sound effect for image analysis process start
        ObjectDetectionSceneOrganiser.Instance.PlaySoundForImageAnalysisStart();

        using (UnityWebRequest analysisWebRequest = UnityWebRequest.Post(predictionEndpoint, string.Empty))
        {
            // The upload handler will help uploading the post data
            byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(GetImageAnalysisPostData(imageCaptureResultData));
            analysisWebRequest.uploadHandler = new UploadHandlerRaw(jsonToSend);

            // The download handler will help receiving the analysis from YOLO server
            analysisWebRequest.downloadHandler = new DownloadHandlerBuffer();

            analysisWebRequest.SetRequestHeader("Content-Type", "application/json");

            // Send the request and wait for the reponse
            yield return analysisWebRequest.SendWebRequest();

            if (analysisWebRequest.isNetworkError || analysisWebRequest.isHttpError)
            {
                Debug.Log("Communication with YOLO server : Network error");
                Debug.Log("Error is " + analysisWebRequest.error);
                ObjectDetectionSceneOrganiser.Instance.ResetDetectionProcessOnError();
            }
            else
            {
                string jsonResponse = analysisWebRequest.downloadHandler.text;

                Debug.Log("response: " + jsonResponse);

                // The response is in JSON format, therefore it needs to be deserialized in our API model
                YOLOObjectDetectionAPIResult analysisResult = new YOLOObjectDetectionAPIResult();
                analysisResult = YOLOObjectDetectionAPIResult.CreateFromJSON(jsonResponse);

                if (analysisResult != null)
                {
                    Debug.Log("#Predictions = " + analysisResult.predictions.Count);

                    // Convert the result from API to domain model
                    YOLOObjectDetectionDomainResult domainResult = YoloObjectDetectionApiToDomainTransformer.TransformYoloObjectDetectionResult(
                        analysisResult, imageCaptureResultData.CameraResolution,
                        imageCaptureResultData.CameraToWorldMatrix,
                        imageCaptureResultData.ProjectionMatrix);

                    // Construct the result data object and pass it to scene organiser to label predicted objects
                    ObjectDetectionAnalysisResultData analysisResultData = new ObjectDetectionAnalysisResultData(
                        domainResult,
                        imageCaptureResultData.CameraResolution,
                        imageCaptureResultData.CameraToWorldMatrix,
                        imageCaptureResultData.ProjectionMatrix
                        );

                    ObjectDetectionSceneOrganiser.Instance.LabelDetectedObjects(analysisResultData);
                }
                else
                {
                    Debug.Log("Predictions is null");
                    // Play sound effect for object detection failure
                    ObjectDetectionSceneOrganiser.Instance.ResetDetectionProcessOnError();
                }
            }
        }
    }

    private string GetImageAnalysisPostData(ImageCaptureResultData imageCaptureResultData)
    {
        // The saved image is given as a Base64 string
        string base64image = Convert.ToBase64String(imageCaptureResultData.Image.ToArray());
        // We also add a flag to denote is the audio description feature is enabled or not
        bool audioDescriptionOn = ObjectDetectionSceneOrganiser.Instance.audioObjectDetectionDescriptionOn;

        ImageAnalysisRequestObject requestObject = new ImageAnalysisRequestObject
        {
            image = base64image,
            predictionsAudioDescriptionEnabled = audioDescriptionOn
        };

        string postDataJson = JsonUtility.ToJson(requestObject);
        return postDataJson;
    }
}
