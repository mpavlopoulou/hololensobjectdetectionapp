﻿using UnityEngine;

// This class is responsible for setting up the cursor in the correct location in real space,
// by making use of the SpatialMappingCollider.
// Based on https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/gaze-in-unity
public class GazeCursor : MonoBehaviour
{
    
    // The cursor (this object) mesh renderer
    private MeshRenderer meshRenderer;

    // Runs at initialization right after the Awake method
    void Start()
    {
        // Grab the mesh renderer that is on the same object as this script.
        meshRenderer = gameObject.GetComponent<MeshRenderer>();

        // Set the gaze cursor reference in scene organiser
        ObjectDetectionSceneOrganiser.Instance.gazeCursor = gameObject;
        gameObject.GetComponent<Renderer>().material.color = Color.green;

        // Change the size of the cursor
        gameObject.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

    // Update is called once per frame
    void Update()
    {
        // Do a raycast into the world based on the user's head position and orientation.
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;

        if (Physics.Raycast(headPosition, gazeDirection, out RaycastHit gazeHitInfo, 30.0f, SpatialMapping.PhysicsRaycastMask))
        {
            // If the raycast hit a hologram, display the cursor mesh.
            meshRenderer.enabled = true;
            // Move the cursor to the point where the raycast hit.
            // Rotate the cursor to hug the surface of the hologram.
            transform.SetPositionAndRotation(gazeHitInfo.point, Quaternion.FromToRotation(Vector3.up, gazeHitInfo.normal));
        }
        else
        {
            // If the raycast did not hit a hologram, hide the cursor mesh.
            meshRenderer.enabled = false;
        }
    }
}
