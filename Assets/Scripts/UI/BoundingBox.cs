﻿using UnityEngine;

// Script for initializing an object detection bounding box, by setting the four corners in 3D space
[RequireComponent(typeof(LineRenderer))]
public class BoundingBox : MonoBehaviour
{
    public void Initialize(int positionsCount, Vector3[] pointsArray)
    {
        // Get a line renderer to connect the four corners
        LineRenderer lineRenderer = gameObject.GetComponent<LineRenderer>();

        // Pick a random, saturated and not-too-dark color for the line
        SetLineSingleColor(lineRenderer, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f));

        // User world space system for positions
        lineRenderer.useWorldSpace = true;

        lineRenderer.loop = true;
        lineRenderer.positionCount = positionsCount;
        lineRenderer.SetPositions(pointsArray);

       
    }

    private void SetLineSingleColor(LineRenderer lineRendererToColor, Color color)
    {
        lineRendererToColor.startColor = color;
        lineRendererToColor.endColor = color;
    }
}