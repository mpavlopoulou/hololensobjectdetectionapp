﻿using Microsoft.MixedReality.Toolkit.Audio;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections.Generic;
using UnityEngine;
using static YoloDetectionDomainModel;

// Sets up the Main Camera by attaching the appropriate components to it.
// When an object is detected, it will be responsible for calculating its position in the real world
// and place a Tooltip Label near it with the appropriate predicted class name.
public class ObjectDetectionSceneOrganiser : MonoBehaviour
{
    // Allows this class to behave like a singleton
    public static ObjectDetectionSceneOrganiser Instance;

    // The gaze cursor object attached to the Main Camera
    internal GameObject gazeCursor;

    // The audio effects player helper object attached to the Main Camera
    internal GameObject audioEffectsPlayerHelper;

    // List of all predicted label objects
    private readonly List<GameObject> predictionLabelObjectsList = new List<GameObject>();
    // List of all predicted bounding box objects
    private readonly List<GameObject> predictedBoundingBoxObjectsList = new List<GameObject>();

    // The game object type that will hold a single prediction label
    [SerializeField]
    private GameObject predictionLabelObject;

    // The game object type that will hold a single prediction bounding box
    [SerializeField]
    private GameObject predictionBoundingBoxObject;

    // The game object type that will hold the collection of all prediction labels
    [SerializeField]
    private GameObject predictionObjectsContainer;

    // Flag to denote if audio object detection results description is on
    [SerializeField]
    public bool audioObjectDetectionDescriptionOn;

    // Flag to denote if object detection process should start automatically
    [SerializeField]
    public bool objectDetectionProcessIsAutomatic;

    // Called on initialization
    private void Awake()
    {
        // Use this class instance as singleton
        Instance = this;

        // Add the ImageCapture class to this Gameobject
        gameObject.AddComponent<ImageCapture>();

        // Add the YoloObjectDetectionAnalyser class to this Gameobject
        gameObject.AddComponent<YoloObjectDetectionAnalyser>();

        // Add the CameraFramesSimilarityCheck class to this Gameobject
        gameObject.AddComponent<CameraFramesSimilarityCheck>();

        // Add the YOLODetectionAPIModel class to this Gameobject
        gameObject.AddComponent<YOLODetectionAPIModel>();
    }

    // Used to place the detected objects labels
    // At then end, reset the capture process to allow the user to capture another image.
    public virtual void LabelDetectedObjects(ObjectDetectionAnalysisResultData detectionResultData)
    {
        // First, clear all the prediction labels and bounding boxes placed from previous analysis result
        ClearPreviousPredictionLabels();
        ClearPreviousPredictionBoundingBoxes();

        IList<YOLODomainPrediction> predictions = detectionResultData.DetectionDomainResult.Predictions;

        if (predictions.Count > 0)
        {
            PlaySoundForObjectDetectionSuccess();
        }
        else
        {
            PlaySoundForObjectDetectionFailure();
        }

        foreach (var prediction in predictions)
        {
            predictionLabelObjectsList.Add(CreatePredictionResultLabel(prediction.TagName, prediction.DetectionCenterWorldPosition));
            predictedBoundingBoxObjectsList.Add(CreatePredictionResultBoundingBox(prediction));
        }

        //If audio description feature is enabled, speak the detection result prediction message
        SpeakObjectDetectionPredictionResultMessage(detectionResultData.DetectionDomainResult);

        if (ShouldDelayDetectionProcessReset())
        {
            _ = new WaitForSeconds(15);
        }
        ResetDetectionProcess();
    }

    // Used to reset the object detection process when an error during image analysis has occurred
    public virtual void ResetDetectionProcessOnError()
    {
        PlaySoundForObjectDetectionFailure();

        ResetDetectionProcessOnError();
    }

    public virtual void ResetDetectionProcess()
    {
        // Reset the color of the cursor
        gazeCursor.GetComponent<Renderer>().material.color = Color.green;

        // Stop the analysis process
        ImageCapture.Instance.ResetImageCapture();
    }


    // For all previously created labels, destroy the game object and clear the list
    private void ClearPreviousPredictionLabels()
    {
        foreach (var label in predictionLabelObjectsList)
        {
            Destroy(label);
        }
        predictionLabelObjectsList.Clear();
    }

    // For all previously created bounding boxes, destroy the game object and clear the list
    private void ClearPreviousPredictionBoundingBoxes()
    {
        foreach (var boundingBox in predictedBoundingBoxObjectsList)
        {
            Destroy(boundingBox);
        }
        predictedBoundingBoxObjectsList.Clear();
    }

    // Create the result label tooltip gameobject
    // Based on https://github.com/microsoft/MixedRealityToolkit-Unity/blob/265a0ea3621c84b88e4b02e7fcd1b5682afef56a/Assets/MRTK/SDK/Features/UX/Scripts/Tooltips/ToolTipSpawner.cs
    private GameObject CreatePredictionResultLabel(string predictionResultClassName, Vector3 predictionResultLocation)
    {
        var labelObject = Instantiate(predictionLabelObject);
        var toolTip = labelObject.GetComponent<ToolTip>();

        toolTip.ToolTipText = predictionResultClassName;
        // point of tooltip rectangle , slightly up from the prediction location
        toolTip.transform.position = predictionResultLocation;
        toolTip.transform.parent = predictionObjectsContainer.transform;

        return labelObject;
    }

    private GameObject CreatePredictionResultBoundingBox(YOLODomainPrediction prediction)
    {
        var boundingBoxObject = Instantiate(predictionBoundingBoxObject);
        var boundingBox = boundingBoxObject.GetComponent<BoundingBox>();

        // Set the bounding box world coordinates
        boundingBox.Initialize(prediction.BoundingBoxCornersNumber, prediction.DetectionBoundingBoxCornerWorldCoordinates);

        boundingBox.transform.parent = predictionObjectsContainer.transform;

        return boundingBoxObject;
    }

    private void SpeakObjectDetectionPredictionResultMessage(YOLOObjectDetectionDomainResult detectionResultModel)
    {
        if (audioObjectDetectionDescriptionOn && !string.IsNullOrEmpty(detectionResultModel.AudioPredictionsDescription))
        {
            TextToSpeech textToSpeech = predictionObjectsContainer.GetComponent<TextToSpeech>();
            var detectionResultMsg = string.Format(detectionResultModel.AudioPredictionsDescription, textToSpeech.Voice.ToString());
            textToSpeech.StartSpeaking(detectionResultMsg);
        }
    }

    // In case the detection process is automate, and audio feedback feature is activated, we delay the reset for a some seconds
    // in case the textToSpeech is still speaking
    private bool ShouldDelayDetectionProcessReset()
    {
        if (objectDetectionProcessIsAutomatic && audioObjectDetectionDescriptionOn)
        {
            TextToSpeech textToSpeech = predictionObjectsContainer.GetComponent<TextToSpeech>();
            return textToSpeech.IsSpeaking();
        }
        return false;
    }

    public void PlaySoundForImageAnalysisStart() => PlaySound(0);

    public void PlaySoundForObjectDetectionSuccess() => PlaySound(1);

    public void PlaySoundForObjectDetectionFailure() => PlaySound(2);

    private void PlaySound(int audioClipIndex)
    {
        if (audioEffectsPlayerHelper != null)
        {
            audioEffectsPlayerHelper.GetComponent<AudioEffectsPlayerHelper>()
                                    .PlayAudio(audioClipIndex);
        }
    }

}