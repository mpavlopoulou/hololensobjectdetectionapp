﻿using UnityEngine;

// CoordinateTransfer: Utility class to help transforming image 2D position of detection results to World 3D position 
// This is done with the help of the Camera to world and projection matrices
// Based on Microsoft docs guidelines: https://github.com/MicrosoftDocs/mixed-reality/blob/a0b2c53dc295db0332832ba00b60bd7a4962e3d9/mixed-reality-docs/locatable-camera.md
namespace Transformers
{
    public static class CoordinateTransfer
    {

        // Get an image 2D (x,y) position and return the position to world coordinates on positionToWorldCoordinates vector
        // Returns true if a valid position is returned to the result vector, false otherwise
        public static bool ImagePositionToWorldPosition(Vector2 pixelCoordinates, Resolution cameraResolution, Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix, out Vector3 positionToWorldCoordinates)
        {
            // The projection transform represents the intrinsic properties of the lens mapped onto an image plane
            // that extends from -1 to +1 in both the X and Y axis.
            Vector2 imagePosZeroToOne = new Vector2(pixelCoordinates.x / cameraResolution.width, 1 - pixelCoordinates.y / cameraResolution.height);
            Vector2 imagePosProjected2D = imagePosZeroToOne * 2.0f - new Vector2(1.0f, 1.0f);  // -1 to 1 space

            Vector3 imagePosProjected = new Vector3(imagePosProjected2D.x, imagePosProjected2D.y, 1);
            Vector3 cameraSpacePos = UnProjectVector(projectionMatrix, imagePosProjected);

            //worldSpaceRayPoint1 is cameraPosition, which is equivalent to the starting point of Ray
            //worldSpaceRayPoint2 is equivalent to the end point of Ray.
            Vector3 cameraLocationWorldSpace = cameraToWorldMatrix.MultiplyPoint(Vector3.zero); // camera location in world space
            Vector3 rayPointWorldSpace = cameraToWorldMatrix.MultiplyPoint(cameraSpacePos); // ray point in world space

            // We call Physics.Raycast to find the collision point between Ray and the real scene, that is, the location of the target object
            // Do a ray cast to the spatial map from the camera position through the location we calculated,
            // basically shooting 'through' the picture for the real object.

            if (Physics.Raycast(cameraLocationWorldSpace, rayPointWorldSpace - cameraLocationWorldSpace, out RaycastHit hit, 20f, SpatialMapping.PhysicsRaycastMask))
            {
                positionToWorldCoordinates = hit.point;
                return true;
            }
            else
            {
                Debug.Log("Collision with the real scene failed!");
            }

            positionToWorldCoordinates = Vector3.zero;
            return false;
        }

        private static Vector3 UnProjectVector(Matrix4x4 proj, Vector3 to)
        {
            Vector3 from = new Vector3(0, 0, 0);
            var axsX = proj.GetRow(0);
            var axsY = proj.GetRow(1);
            var axsZ = proj.GetRow(2);
            from.z = to.z / axsZ.z;
            from.y = (to.y - (from.z * axsY.z)) / axsY.y;
            from.x = (to.x - (from.z * axsX.z)) / axsX.x;
            return from;
        }
    }
}
