﻿using System.Collections.Generic;
using UnityEngine;

//This script allows us to toggle music to play and stop.
//Assign an AudioSource to the GameObject.
public class AudioEffectsPlayerHelper : MonoBehaviour
{
    private AudioSource audioSource;

    // A list with audio clips we wish to play
    public List<AudioClip> audioClips;

    // Start is called before the first frame update
    void Start()
    {
        // Set the audio effects player helper reference in scene organiser
        ObjectDetectionSceneOrganiser.Instance.audioEffectsPlayerHelper = gameObject;

        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAudio(int audioClipIndex)
    {
        if (audioSource != null && audioClips != null && audioClipIndex >= 0 && audioClipIndex < audioClips.Count)
        {
            audioSource.clip = audioClips[audioClipIndex];
            audioSource.Play();
        }
    }
}