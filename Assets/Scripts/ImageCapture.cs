﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.WSA.Input;
using UnityEngine.Windows.WebCam;
using System.Collections.Generic;
using System.Collections;

// ImageCapture class is responsible for:
// 1) Capturing an image using the HoloLens camera and sending it along with camera resolution and a copy of the Camera's transform for analysis.
// 2) Handling Tap gestures from the user.
// Based on https://docs.unity3d.com/560/Documentation/ScriptReference/VR.WSA.WebCam.PhotoCapture.html
public class ImageCapture : MonoBehaviour
{
    // Allows this class to behave like a singleton
    public static ImageCapture Instance;

    // Photo Capture object
    private PhotoCapture photoCaptureObject = null;

    // Image capture result data
    private ImageCaptureResultData imageCaptureResultData = null;

    // Camera resolution object
    private Resolution cameraResolution;

    // Allows gestures recognition in HoloLens
    private GestureRecognizer recognizer;

    // The period to wait for the next automatic image capture
    private readonly WaitForSeconds automaticImageCapturePeriod = new WaitForSeconds(30f);

    // Flagging if the capture loop is running
    internal bool captureIsActive;

    // Called on initialization
    private void Awake() => Instance = this;


    // Runs at initialization right after Awake method
    [Obsolete]
    void Start()
    {
        // Subscribing to the Microsoft HoloLens API gesture recognizer to track user tap gestures
        // Only if the object detection process does not start automatically
        if (!ObjectDetectionSceneOrganiser.Instance.objectDetectionProcessIsAutomatic)
        {
            recognizer = new GestureRecognizer();
            recognizer.SetRecognizableGestures(GestureSettings.Tap);
            recognizer.Tapped += TapHandler;
            recognizer.StartCapturingGestures();
        }
        else
        {
            StartCoroutineForPeriodicFrameSimilarityChecks();
        }

        // Set the camera resolution to be the highest possible
        cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending
            ((res) => res.width * res.height).First();
    }


    // Respond to Tap Input.
    private void TapHandler(TappedEventArgs obj) => StartImageCaptureProcess();

    private void StartCoroutineForPeriodicFrameSimilarityChecks()
    {
        StartCoroutine(nameof(PeriodicFrameSimilarityChecks));
    }

    private IEnumerator PeriodicFrameSimilarityChecks()
    {
        while (true)
        {
            // Start the image capture process without a player action and wait for the period time
            StartImageCaptureProcess();
            yield return automaticImageCapturePeriod;
        }
    }

    public void StartImageCaptureProcess()
    {
        // When a tap gesture is identified, we respond to it only if the camera capture is not active
        if (!captureIsActive)
        {
            captureIsActive = true;

            // Set the cursor color to red, to denote that the camera is busy.
            // When the cursor is green, it means the camera is available to take the image.
            if (ObjectDetectionSceneOrganiser.Instance.gazeCursor != null)
            {
                ObjectDetectionSceneOrganiser.Instance.gazeCursor.GetComponent<Renderer>().material.color = Color.red;
            }

            // Begin the capture process
            Invoke(nameof(ExecuteImageCaptureAndAnalysis), 0);
        }
    }

    // Begin process of image capturing and send to our Object Detection Service for analysis.
    private void ExecuteImageCaptureAndAnalysis()
    {
        // Begin capture process, set the image format (without showing holograms)
        PhotoCapture.CreateAsync(false, delegate (PhotoCapture captureObject)
        {
            photoCaptureObject = captureObject;

            CameraParameters cameraParameters = new CameraParameters
            {
                hologramOpacity = 0.0f,
                cameraResolutionWidth = cameraResolution.width,
                cameraResolutionHeight = cameraResolution.height,
                pixelFormat = CapturePixelFormat.JPEG
            };

            // Activate the camera
            photoCaptureObject.StartPhotoModeAsync(cameraParameters, delegate (PhotoCapture.PhotoCaptureResult result)
            {
                // Take a picture
                photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
            });
        });
    }


    // Register the full execution of the Photo Capture. 
    void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        // Take camera to world and projection matrix
        bool isCameraToWorldMatrixValid = photoCaptureFrame.TryGetCameraToWorldMatrix(out Matrix4x4 cameraToWorldMatrix);
        bool isProjectionMatrixValid = photoCaptureFrame.TryGetProjectionMatrix(Camera.main.nearClipPlane, Camera.main.farClipPlane,out Matrix4x4 projectionMatrix);
        Debug.Log("isCameraToWorldMatrixValid is " + isCameraToWorldMatrixValid + " and isProjectionMatrixValid is " + isProjectionMatrixValid);

        // Copy raw image data to photoBuffer
        var photoBuffer = new List<byte>();
        if (photoCaptureFrame.pixelFormat == CapturePixelFormat.JPEG)
        {
            photoCaptureFrame.CopyRawImageDataIntoBuffer(photoBuffer);
        }

        if (isCameraToWorldMatrixValid && isProjectionMatrixValid && photoBuffer != null)
        {
            imageCaptureResultData = new ImageCaptureResultData(photoBuffer, cameraResolution, cameraToWorldMatrix, projectionMatrix);
        }

        // Call StopPhotoMode once the image has successfully captured to deactivate our camera
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }

    // The camera photo mode has stopped after the capture.
    // Begin the image analysis process.
    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        Debug.LogFormat("Stopped Photo Mode");

        // Dispose from the object in memory and request the image analysis 
        photoCaptureObject.Dispose();
        photoCaptureObject = null;

        if (imageCaptureResultData != null)
        {
            if (!ObjectDetectionSceneOrganiser.Instance.objectDetectionProcessIsAutomatic)
            {
                // Call the image analysis
                StartCoroutine(YoloObjectDetectionAnalyser.Instance.AnalyseLastImageCaptured(imageCaptureResultData));
            }
            else
            {
                // Call the camera frames similarity check
                StartCoroutine(CameraFramesSimilarityCheck.Instance.CheckPreviousAndCurrentFramesCapturedForSimilarity(imageCaptureResultData));
            }

            // Clear result data
            imageCaptureResultData = null;
        }
        else
        {
            ResetImageCapture();
        }
    }

    // Stops all capture pending actions
    internal void ResetImageCapture()
    {
        captureIsActive = false;

        // Set the cursor color to green
        ObjectDetectionSceneOrganiser.Instance.gazeCursor.GetComponent<Renderer>().material.color = Color.green;

        // Stop the capture loop if active
        CancelInvoke();
    }
}