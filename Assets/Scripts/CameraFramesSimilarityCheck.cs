﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using static CameraFrameSimilarityCheckRequest;
using static CameraFramesSimilarityAPIModel;

public class CameraFramesSimilarityCheck : MonoBehaviour
{
    // Unique instance of this class
    public static CameraFramesSimilarityCheck Instance;

    //Jetson TX2 camera frame endpoint
    private readonly string framesSimilarityEndpoint = "http://tesla2.di.uoa.gr:8081/HoloLensObjectDetection/checkFramesSimilarity";

    private IList<byte> PreviousCameraFrameData;

    // Initializes this class
    private void Awake()
    {
        // Allows this instance to behave like a singleton
        Instance = this;
        // Initialize the previous camera frame data to null
        PreviousCameraFrameData = null;
    }

    // Call our Camera Frames Similarity check Service to submit the previous and current frames data.
    // Obtains the result of the frames similarity with a boolean flag which denotes if frames are similar or not.
    public IEnumerator CheckPreviousAndCurrentFramesCapturedForSimilarity(ImageCaptureResultData currentImageCaptureResultData)
    {
        Debug.Log("Checking camera frames similarity...");

        if (PreviousCameraFrameData == null)
        {
            // In case this similarity check has not been called previously, start the object detection API call,
            // and save the current image frame for the next call of frames similarity
            PreviousCameraFrameData = currentImageCaptureResultData.Image;
            StartCoroutine(YoloObjectDetectionAnalyser.Instance.AnalyseLastImageCaptured(currentImageCaptureResultData));
        }
        else
        {
            using (UnityWebRequest framesSimilarityWebRequest = UnityWebRequest.Post(framesSimilarityEndpoint, string.Empty))
            {
                // The upload handler will help uploading the post data
                byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(GetFramesSimilarityPostData(currentImageCaptureResultData));
                framesSimilarityWebRequest.uploadHandler = new UploadHandlerRaw(jsonToSend);

                // The download handler will help receiving the analysis from camera frames similarity API call
                framesSimilarityWebRequest.downloadHandler = new DownloadHandlerBuffer();

                framesSimilarityWebRequest.SetRequestHeader("Content-Type", "application/json");

                // Send the request and wait for the reponse
                yield return framesSimilarityWebRequest.SendWebRequest();

                if (framesSimilarityWebRequest.isNetworkError || framesSimilarityWebRequest.isHttpError)
                {
                    Debug.Log("Communication with server for frames similarity : Network error");
                    Debug.Log("Error is " + framesSimilarityWebRequest.error);
                    ObjectDetectionSceneOrganiser.Instance.ResetDetectionProcessOnError();
                }
                else
                {
                    string jsonResponse = framesSimilarityWebRequest.downloadHandler.text;

                    Debug.Log("frames similarity response: " + jsonResponse);

                    // The response is in JSON format, therefore it needs to be deserialized in our API model
                    CameraFramesSimilarityAPIResult similarityCheckResult = new CameraFramesSimilarityAPIResult();
                    similarityCheckResult = CameraFramesSimilarityAPIResult.CreateFromJSON(jsonResponse);

                    if (similarityCheckResult != null)
                    {
                        // Save the current image frame for the next call of frames similarity
                        PreviousCameraFrameData = currentImageCaptureResultData.Image;
                        // The frames are not similar, hence start the object detection API call for the next frame
                        if (!similarityCheckResult.areFramesSimilar)
                        {
                            StartCoroutine(YoloObjectDetectionAnalyser.Instance.AnalyseLastImageCaptured(currentImageCaptureResultData));
                        }
                        else
                        {
                            ObjectDetectionSceneOrganiser.Instance.ResetDetectionProcess();
                        }
                    }
                    else
                    {
                        Debug.Log("Similarity check response is null");
                        // Play sound effect for object detection failure
                        ObjectDetectionSceneOrganiser.Instance.ResetDetectionProcessOnError();
                    }
                }
            }
        }
    }

    private string GetFramesSimilarityPostData(ImageCaptureResultData currentImageCaptureResultData)
    {
        // The previous saved image camera frame is given as a Base64 string
        string previousFrameBase64image = Convert.ToBase64String(PreviousCameraFrameData.ToArray());

        // The current saved image is given as a Base64 string
        string currentFrameBase64image = Convert.ToBase64String(currentImageCaptureResultData.Image.ToArray());

        CameraFrameSimilarityCheckRequestObject requestObject = new CameraFrameSimilarityCheckRequestObject
        {
            previous_frame_image = previousFrameBase64image,
            current_frame_image = currentFrameBase64image
        };

        string postDataJson = JsonUtility.ToJson(requestObject);
        return postDataJson;
    }
}
